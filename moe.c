#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <php.h>
#include <ext/standard/info.h>
#include <Zend/zend_extensions.h>
/*
 * snipets

// declare arginfo
ZEND_BEGIN_ARG_INFO_EX(arginfo_git_get_object, 0, 0, 1)
    ZEND_ARG_INFO(0, hash)
ZEND_END_ARG_INFO()

// get object related structs
php_moe *this = (php_moe *) zend_object_store_get_object(getThis() TSRMLS_CC);

*/

ZEND_BEGIN_ARG_INFO_EX(arginfo_moe_fib, 0, 0, 1)
    ZEND_ARG_INFO(0, num)
ZEND_END_ARG_INFO()


PHPAPI zend_class_entry *moe_class_entry;

typedef struct{
    zend_object zo;
    zend_fcall_info *fci;
    zend_fcall_info_cache *fcc;
    zend_fcall_info *fci2;
    zend_fcall_info_cache *fcc2;
} php_moe;

static void php_moe_free_storage(php_moe *obj TSRMLS_DC)
{
    zend_object_std_dtor(&obj->zo TSRMLS_CC);
    efree(obj->fci);
    efree(obj->fcc);
    efree(obj);
}

zend_object_value php_moe_new(zend_class_entry *ce TSRMLS_DC)
{
	zend_object_value retval;
	php_moe *obj;
	zval *tmp;

	obj = ecalloc(1, sizeof(*obj));
	zend_object_std_init( &obj->zo, ce TSRMLS_CC );
	zend_hash_copy(obj->zo.properties, &ce->default_properties, (copy_ctor_func_t) zval_add_ref, (void *) &tmp, sizeof(zval *));

	retval.handle = zend_objects_store_put(obj, 
        (zend_objects_store_dtor_t)zend_objects_destroy_object,
        (zend_objects_free_object_storage_t)php_moe_free_storage,
        NULL TSRMLS_CC);
	retval.handlers = zend_get_std_object_handlers();

	return retval;
}

unsigned long result[48];
unsigned long fibonatch(int n);

unsigned long fibonatch(int n)
{
    unsigned long f;
    if(f = result[n])
        return f;
    
    switch(n){
        case 1:
        case 2:
            f = 1L;
            break;
        default:
            f = fibonatch(n -1) + fibonatch(n - 2);
            break;
    }
    result[n] = f;
    return f;
}

PHP_METHOD(moe, fib)
{
    long num = 10;
    unsigned long result = 0;
    if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,
        "|l", &num) == FAILURE){
        return;
    }
    result = fibonatch(num);

    RETURN_LONG(result);
}


PHP_METHOD(moe, fib_fcall)
{
    php_moe *this = (php_moe *) zend_object_store_get_object(getThis() TSRMLS_CC);
    long num = 0;
    unsigned long result = 0;
    if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,
        "l", &num) == FAILURE){
        return;
    }
    zval *retval;// = emalloc(sizeof(zval));
	zval *params[1];
	MAKE_STD_ZVAL(retval);
	ZVAL_NULL(retval);

	MAKE_STD_ZVAL(params[0]);
    ZVAL_LONG(params[0],num);
    this->fci->params = NULL;//params指定するとせぐふぉっちゃう＞＜
    this->fci->param_count = 0;

	zend_fcall_info_call(this->fci,this->fcc,&retval,NULL TSRMLS_CC);
	zval_ptr_dtor(&retval);
	zval_ptr_dtor(&params[0]);
}



PHP_METHOD(moe, fib_callu)
{
    php_moe *this = (php_moe *) zend_object_store_get_object(getThis() TSRMLS_CC);
    long num = 0;
    unsigned long result = 0;
    if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,
        "l", &num) == FAILURE){
        return;
    }
	zval *retval;
	zval *params[1];
    zval func;

	MAKE_STD_ZVAL(retval);
	ZVAL_NULL(retval);
    MAKE_STD_ZVAL(params[0]);
    ZVAL_LONG(params[0],num);
    zval *obj = getThis();
    ZVAL_STRING(&func,"fib", 1);

    call_user_function(NULL,&obj,&func,retval,1,params TSRMLS_CC);

	zval_ptr_dtor(&retval);
	zval_ptr_dtor(&params[0]);
}

PHP_METHOD(moe, fib_zend)
{
    php_moe *this = (php_moe *) zend_object_store_get_object(getThis() TSRMLS_CC);
    long num = 0;
    unsigned long result = 0;
    if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,
        "l", &num) == FAILURE){
        return;
    }
    zval *retval;// = emalloc(sizeof(zval));
	zval *params[1];
	MAKE_STD_ZVAL(retval);
	ZVAL_NULL(retval);

	MAKE_STD_ZVAL(params[0]);
    ZVAL_LONG(params[0],num);

    this->fci->retval_ptr_ptr = &retval;
    this->fci->params = NULL;
    this->fci->param_count = 0;
    
	zend_call_function(this->fci,this->fcc TSRMLS_CC);
	zval_ptr_dtor(&retval);
	zval_ptr_dtor(&params[0]);
}

PHP_METHOD(moe, __construct)
{
    php_moe *this = (php_moe *) zend_object_store_get_object(getThis() TSRMLS_CC);
    this->fci = emalloc(sizeof(zend_fcall_info));
    this->fcc = emalloc(sizeof(zend_fcall_info_cache));

    zval *funcname = emalloc(sizeof(zval));
	ZVAL_STRING(funcname,"fib",1);

	this->fci->size = sizeof(zend_fcall_info);
	this->fci->function_table = NULL;
	this->fci->function_name = funcname;
	this->fci->symbol_table = NULL;
	this->fci->object_ptr = getThis();
	this->fci->retval_ptr_ptr = NULL;
	this->fci->param_count = 1;
	this->fci->params = NULL;
	this->fci->no_separation = 1;

	zend_class_entry *obj_ce = Z_OBJCE_P(getThis());
	this->fcc->initialized = 0;
	this->fcc->function_handler = NULL;
	this->fcc->calling_scope = obj_ce;
	this->fcc->called_scope = obj_ce;
	this->fcc->object_ptr = getThis();
    
    zval *call = emalloc(sizeof(zval));
	ZVAL_STRING(call,"Moe::fib",1);

    char *error;
    zend_fcall_info_init(call,0,this->fci2,this->fcc2,NULL,&error TSRMLS_CC);
}

PHPAPI function_entry php_moe_methods[] = {
    PHP_ME(moe,__construct,NULL,ZEND_ACC_PUBLIC)
    PHP_ME(moe,fib,arginfo_moe_fib,ZEND_ACC_PUBLIC)
    PHP_ME(moe,fib_callu,arginfo_moe_fib,ZEND_ACC_PUBLIC)
    PHP_ME(moe,fib_fcall,arginfo_moe_fib,ZEND_ACC_PUBLIC)
    PHP_ME(moe,fib_zend,arginfo_moe_fib,ZEND_ACC_PUBLIC)
    {NULL, NULL, NULL}
};

PHP_MINIT_FUNCTION(moe) {
    zend_class_entry ce;
    INIT_CLASS_ENTRY(ce, "Moe", php_moe_methods);
    moe_class_entry = zend_register_internal_class(&ce TSRMLS_CC);
    moe_class_entry->create_object = php_moe_new;
}

static zend_module_entry moe_module_entry = {
	STANDARD_MODULE_HEADER,
	"moe",
	NULL,
	PHP_MINIT(moe),
	NULL,
	NULL,
	NULL,
	NULL,
	"0.0.0",
	STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_MOE
ZEND_GET_MODULE(moe)
#endif
