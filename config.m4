PHP_ARG_ENABLE(moe, whether to enable moe support,
[  --enable-moe           Enable moe support])
if test "$PHP_MOE" != "no"; then
  PHP_NEW_EXTENSION(moe, moe.c, $ext_shared)
fi