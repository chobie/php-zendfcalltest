<?php
$moe = new Moe();
$count = 10000;

$start[0] = microtime(true);
for($i=0;$i<$count;$i++){
    $moe->fib_zend(10);
}
$end[0] = microtime(true);
printf("[zend_call]%0.8f\n",$end[0]-$start[0]);

$start[1] = microtime(true);
for($i=0;$i<$count;$i++){
    $moe->fib(10);
}
$end[1] = microtime(true);
printf("[direct call]%0.8f\n",$end[1]-$start[1]);


$start[2] = microtime(true);
for($i=0;$i<$count;$i++){
    $moe->fib_callu(10);
}
$end[2] = microtime(true);
printf("[zend_callu]%0.8f\n",$end[2]-$start[2]);


$start[3] = microtime(true);
for($i=0;$i<$count;$i++){
    $moe->fib_fcall(10);
}
$end[3] = microtime(true);
printf("[zend_fcall]%0.8f\n",$end[3]-$start[3]);

